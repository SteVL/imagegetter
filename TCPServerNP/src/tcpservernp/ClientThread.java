package tcpservernp;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import org.apache.log4j.Logger;

/**
 *
 * @author SteVL
 */
public class ClientThread implements Runnable {

    public static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ClientThread.class);

    private Socket socket;

    private DataInputStream dis;
    private DataOutputStream dos;

    public ClientThread(Socket socket) {
        this.socket = socket;
    }

    /**
     * ����� ��� ������������ ����������.
     */
    private void testConnection() {
        String inputMsg, outputMsg;
        System.out.println("Client connected");
        System.out.println("Waiting for messages...");

        while (socket != null) {
            try {

                dis = new DataInputStream(socket.getInputStream());
                dos = new DataOutputStream(socket.getOutputStream());

                while (true) {
                    inputMsg = dis.readUTF();
                    System.out.println("client: " + inputMsg);
                    if (inputMsg.equalsIgnoreCase("files/list-1.txt")) {
                        outputMsg = "file exist";
                    } else {
                        outputMsg = "file doesn't exist";
                    }
                    dos.writeUTF("Server: " + inputMsg + " " + outputMsg);
                }
            } catch (IOException ex) {
                System.out.println("Connection was lost.");
            } finally {
                try {
                    dis.close();
                    dos.close();
                    socket.close();
                    socket = null;
                    System.out.println("Client was disconnected.");
                } catch (IOException ex) {
                    //
                }
            }
        }
    }

    @Override
    public void run() {
        System.out.println("Client connected");
        System.out.println("Waiting for messages...");
        while (socket != null) {
            if (log.isDebugEnabled()) {
                log.debug(socket);
            }
            try {
                dis = new DataInputStream(socket.getInputStream());
                if (log.isDebugEnabled()) {
                    log.debug("DataInputStream was created");
                    log.debug(dis);
                    log.debug(socket.getInputStream());
                }
                dos = new DataOutputStream(socket.getOutputStream());
                if (log.isDebugEnabled()) {
                    log.debug("DataOutputStream was created");
                    log.debug(dos);
                    log.debug(socket.getOutputStream());
                }
                /*�������
                ��� �������� �������, ��� ����������� ������ - ���� ��� �����������?
                ���� fileName=="", �� ������������ �����������
                 */

                while (true) {
                    //�������� ������ �� ����                   
                    String fileName = dis.readUTF();
                    if (!fileName.isEmpty()) {
                        if (log.isDebugEnabled()) {
                            log.debug("Server got fileName:" + fileName);
                        }
                        //��������� ����
                        sendFile(fileName);
                        //���������� ������ �����������
                        sendImage(dis.readUTF());
                    } else {
                        //������������ ����������� ����� ����, ��� ���� ��������� ������ �����������
                        //� ������ �������� ��������� ��� ����������.
                        sendImage(dis.readUTF());
                    }
                    if (log.isDebugEnabled()) {
                        log.debug("Server sent response.");
                    }
                }
            } catch (IOException ex) {
                System.out.println("Connection was lost.");
            } finally {
                try {
                    dis.close();
                    dos.close();
                    socket.close();
                    socket = null;
                    System.out.println("Client was disconnected.");
                } catch (IOException ex) {
                    if (log.isDebugEnabled()) {
                        log.debug("****ERROR: " + ex);
                    }
                }
            }
        }
    }

    /**
     * ���������� ���� �� ������� ����������� �������.
     *
     * @param fileName
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void sendFile(String fileName) throws FileNotFoundException, IOException {
        File file = new File("files/" + fileName);
        if (!file.exists()) {
            dos.writeBoolean(false);
            if (log.isDebugEnabled()) {
                log.debug("File " + fileName + " wasn't found on the server.");
            }
            return;
        } else {
            //���� ����������
            dos.writeBoolean(true);
        }
        send(file);
    }

    /**
     * ���������� ����������� �������.
     *
     * @param imageTitle
     * @throws IOException
     */
    private void sendImage(String imageTitle) throws IOException {
        File file = new File("images/" + imageTitle);
        if (!file.exists()) {
            dos.writeBoolean(false);
            if (log.isDebugEnabled()) {
                log.debug("image " + imageTitle + " wasn't found on the server.");
            }
            return;
        } else {
            dos.writeBoolean(true);
        }
        send(file);
        if (log.isDebugEnabled()) {
            log.debug("image " + imageTitle + " was sent.");
        }
    }

    /**
     * ���������� ����: ���� �� ������� ����������� ��� ���� �����������.
     *
     * @param file
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void send(File file) throws FileNotFoundException, IOException {
        InputStream is = new BufferedInputStream(new FileInputStream(file));
        if (log.isDebugEnabled()) {
            log.debug("File was opened for reading.");
        }
        byte[] buffer = new byte[1024];

        int readBytes = 0;
        if (log.isDebugEnabled()) {
            log.debug("Starting sending file.");
        }
        dos.writeLong(file.length());
        if (log.isDebugEnabled()) {
            log.debug("Filesize " + file.length() + " bytes was sent.");
        }

        while ((readBytes = is.read(buffer)) != -1) {
            dos.write(buffer, 0, readBytes);
        }

        dos.flush();
        if (log.isDebugEnabled()) {
            log.debug("File was sent.");
        }
        is.close();
    }
}
