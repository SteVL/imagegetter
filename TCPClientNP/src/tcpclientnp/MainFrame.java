package tcpclientnp;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author stud
 */
public class MainFrame extends javax.swing.JFrame {

    public static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MainFrame.class);
    public static final String APP_TITLE = "Lab2 - ImageGetter";

    private static final String DIR_FILES = "files";
    private static final String DIR_IMAGES = "images";

    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;

    private ArrayList<String> arrTitles;
    /**
     * ���������. ����� �������� ����������� �� ������� �� �����.
     */
    private int currentIndex;

    private javax.swing.JLabel jLabel = new javax.swing.JLabel();

    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        initComponents();
        setLocationRelativeTo(null);
        imageTitleLabel.setText("");
        currentCountLabel.setText("0");
        totalCountLabel.setText("0");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        fileNameInput = new javax.swing.JTextField();
        requestButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        addressInput = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();
        currentCountLabel = new javax.swing.JLabel();
        imageTitleLabel = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        totalCountLabel = new javax.swing.JLabel();
        previousImgBtn = new javax.swing.JButton();
        nextImageBtn = new javax.swing.JButton();
        jScrollPane = new javax.swing.JScrollPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Lab2 - ImageGetter");

        jLabel1.setText("����:");

        fileNameInput.setText("list-1.txt");
        fileNameInput.setToolTipText("��� ����� �� ������� ��� �����������.");

        requestButton.setText("������");
        requestButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                requestButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("�����:");

        addressInput.setText("127.0.0.1");

        jLabel4.setText("��������:");

        currentCountLabel.setText("0");

        imageTitleLabel.setText("olololoolololololololoolololololololololololololololololololololololololololololololololololo.jpg");
        imageTitleLabel.setToolTipText("");

        jLabel7.setText("��");

        totalCountLabel.setText("10");

        previousImgBtn.setText("<  ����������");
        previousImgBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                previousImgBtnActionPerformed(evt);
            }
        });

        nextImageBtn.setText("���������  >");
        nextImageBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextImageBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(fileNameInput, javax.swing.GroupLayout.DEFAULT_SIZE, 439, Short.MAX_VALUE)
                                    .addComponent(addressInput))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(requestButton))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(imageTitleLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(currentCountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(totalCountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(171, 171, 171)
                        .addComponent(previousImgBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nextImageBtn)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(fileNameInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(requestButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(addressInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(currentCountLabel)
                    .addComponent(imageTitleLabel)
                    .addComponent(jLabel7)
                    .addComponent(totalCountLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 355, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(previousImgBtn)
                    .addComponent(nextImageBtn))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    /**
     * �������� ������� �� ��������� ����� �� ������� �����������.
     *
     * @param evt
     */
    private void requestButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_requestButtonActionPerformed
        String fileName = fileNameInput.getText();
        if (!fileName.endsWith(".txt")) {
            JOptionPane.showMessageDialog(this, "���� ������ ����� ���������� .txt", APP_TITLE, 1);
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("fileName:" + fileName);
        }

        socket = null;
        try {
            socket = new Socket(addressInput.getText(), 1234);
            if (log.isDebugEnabled()) {
                log.debug("Socket was created");
                log.debug(socket);
            }
            dos = new DataOutputStream(socket.getOutputStream());
            if (log.isDebugEnabled()) {
                log.debug("DataOutputStream was created");
                log.debug(dos);
                log.debug(socket.getOutputStream());
            }
            dis = new DataInputStream(socket.getInputStream());
            if (log.isDebugEnabled()) {
                log.debug("DataInputStream was created");
                log.debug(dis);
                log.debug(socket.getInputStream());
            }

            System.out.println("Connection was set");
            System.out.println("Proccessing of request...");
            if (log.isDebugEnabled()) {
                log.debug("Connection is OK. Sending/recieving data.");
            }

            dos.writeUTF(fileName);
            if (log.isDebugEnabled()) {
                log.debug("fileName " + fileName + " was sent.");
            }

            //��������� ������ � ������� �����.
            if (dis.readBoolean() == false) {
                if (log.isDebugEnabled()) {
                    log.debug("File wasn't found on the server.");
                }
                JOptionPane.showMessageDialog(this, "���� � ������ \"" + fileName + "\" �� ������.", APP_TITLE, JOptionPane.INFORMATION_MESSAGE);
                return;
            }

            downloadFile(fileName);
            System.out.println("File was got.");
            JOptionPane.showMessageDialog(this, "���� ������� ��������.", APP_TITLE, JOptionPane.INFORMATION_MESSAGE);
            if (log.isDebugEnabled()) {
                log.debug("Client got response about file.");
            }
            
            arrTitles = readFileWithImg(fileName);
            for (int i = 0; i < arrTitles.size(); i++) {
                System.out.println(arrTitles.get(i));
            }
            if (log.isDebugEnabled()) {
                log.debug("Array of titles was built.");
            }
            deleteFile(DIR_FILES,fileName);
            
            //��������� ������� ����� �����������
            totalCountLabel.setText(String.valueOf(arrTitles.size()));
            //������� ������ ����������� �� ������
            imageTitleLabel.setText(arrTitles.get(0));

            //����� ������ �� �����������
            downloadImage(arrTitles.get(0));
            showImage(arrTitles.get(0));
            deleteFile(DIR_IMAGES,arrTitles.get(0));
            //������ �� �����������
            
            //��������� ������ �����������
            currentIndex = 0;
            currentCountLabel.setText(String.valueOf(1));

            if (log.isDebugEnabled()) {
                log.debug("Socket closing...");
            }
            dis.close();
            dos.close();
            socket.close();
            if (log.isDebugEnabled()) {
                log.debug("Socket was closed.");
            }
        } catch (IOException ex) {
            if (log.isDebugEnabled()) {
                log.debug("Error:********* " + ex);
            }
            JOptionPane.showMessageDialog(null, "There is no connection. Offline mode.");
        }
    }//GEN-LAST:event_requestButtonActionPerformed
    /**
     * ��������� ���� �� ������� �����������
     *
     * @param fileName
     * @throws IOException
     */
    private void downloadFile(String fileName) throws IOException {
        File dir = new File(DIR_FILES);
        if (!dir.exists()) {
            dir.mkdir();
        }
        recieveFile(dir, fileName);
    }

    /**
     * ��������� ����� � �������.
     *
     * @param dir
     * @param fileName
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void recieveFile(File dir, String fileName) throws FileNotFoundException, IOException {
        final int SIZE = 1024;
        OutputStream os = new BufferedOutputStream(new FileOutputStream(dir + "/" + fileName));
        if (log.isDebugEnabled()) {
            log.debug("File was opened for writing.");
        }
        byte[] buffer = new byte[SIZE];
        int readBytes = 0;
        int totalBytes = 0;
        if (log.isDebugEnabled()) {
            log.debug("Starting recieving file.");
        }
        long fileSize = dis.readLong();
        if (log.isDebugEnabled()) {
            log.debug("Filesize was got:" + fileSize + " bytes");
        }
        while ((readBytes = dis.read(buffer)) != -1) {
            os.write(buffer, 0, readBytes);
            totalBytes += readBytes;
            if (totalBytes == fileSize) {
                break;
            }
        }
        os.flush();
        if (log.isDebugEnabled()) {
            log.debug("File was written.");
        }
        os.close();
    }

    /**
     * ������ ���� � ���������� �����������.
     *
     * @param fileName
     * @return ������ �������� ����������� ��� ������������ �������.
     * @throws FileNotFoundException
     * @throws IOException
     */
    private ArrayList<String> readFileWithImg(String fileName) throws FileNotFoundException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("File reading: " + fileName);
        }
        BufferedReader buf=new BufferedReader(new FileReader(DIR_FILES + "/" + fileName));
        ArrayList<String> arrTitles = new ArrayList<String>();
        String title = new String();
        while ((title = buf.readLine()) != null) {
            arrTitles.add(title);
        }
        buf.close();
        if (log.isDebugEnabled()) {
            log.debug("File reading was finished.");
        }
        return arrTitles;
    }

    /**
     * ����������� ��������� �����������.
     *
     * @param evt
     */
    private void nextImageBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextImageBtnActionPerformed
        if (arrTitles == null || (currentIndex >= arrTitles.size() - 1)) {
            return;
        }

        //��������� ����� ���������� ����������� �� ������        
        currentCountLabel.setText(String.valueOf(currentIndex + 2));
        //����������� ��������� ����������� �� �������
        //����� ������ �� �����������
        String imageTitle = arrTitles.get(currentIndex + 1);
        downloadImage(imageTitle);
        showImage(imageTitle);
        deleteFile(DIR_IMAGES,imageTitle);
        //������ �� �����������
        imageTitleLabel.setText(arrTitles.get(currentIndex + 1));

        currentIndex++;
    }//GEN-LAST:event_nextImageBtnActionPerformed
    /**
     * ����������� ����������� �����������.
     *
     * @param evt
     */
    private void previousImgBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_previousImgBtnActionPerformed
        if (arrTitles == null || currentIndex <= 0) {
            return;
        }

        //��������� ����� ����������� ����������� �� ������      
        currentCountLabel.setText(String.valueOf(currentIndex));
        //����������� ���������� ����������� �� �������
        //����� ������ �� �����������
        String imageTitle = arrTitles.get(currentIndex - 1);
        downloadImage(imageTitle);
        showImage(imageTitle);
        deleteFile(DIR_IMAGES,imageTitle);
        //������ �� �����������      
        imageTitleLabel.setText(arrTitles.get(currentIndex - 1));
        currentIndex--;
    }//GEN-LAST:event_previousImgBtnActionPerformed
    /**
     * ��������� ����������� � �������.
     *
     * @param imageTitle
     */
    private void downloadImage(String imageTitle) {
        try {
            socket = new Socket(addressInput.getText(), 1234);
            if (log.isDebugEnabled()) {
                log.debug("Socket was created");
                log.debug(socket);
            }
            dos = new DataOutputStream(socket.getOutputStream());
            if (log.isDebugEnabled()) {
                log.debug("DataOutputStream was created");
                log.debug(dos);
                log.debug(socket.getOutputStream());
            }
            dis = new DataInputStream(socket.getInputStream());
            if (log.isDebugEnabled()) {
                log.debug("DataInputStream was created");
                log.debug(dis);
                log.debug(socket.getInputStream());
            }

            System.out.println("Connection was set");
            System.out.println("Proccessing of request...");
            if (log.isDebugEnabled()) {
                log.debug("Connection is OK. Sending/recieving data.");
            }
            //����� ������ �� �����������..

            //�������� ������� ������, ��� ����� �����������, � �� ����
            dos.writeUTF("");
            if (log.isDebugEnabled()) {
                log.debug("Empty string was sent, to get images.");
            }
            dos.writeUTF(imageTitle);
            //��������� ������ � ������� �����������.
            if (dis.readBoolean() == false) {
                if (log.isDebugEnabled()) {
                    log.debug("Image" + arrTitles.get(0) + " wasn't found on the server.");
                }
                JOptionPane.showMessageDialog(this, "����������� � ������ \"" + arrTitles.get(0) + "\" �� �������.", APP_TITLE, JOptionPane.WARNING_MESSAGE);
                return;
            }

            File dir = new File(DIR_IMAGES);
            if (!dir.exists()) {
                dir.mkdir();
            }
            recieveFile(dir, imageTitle);
            if (log.isDebugEnabled()) {
                log.debug("image " + imageTitle + " was got.");
            }
            //..������ �� �����������
            if (log.isDebugEnabled()) {
                log.debug("Socket closing...");
            }
            dis.close();
            dos.close();
            socket.close();
            if (log.isDebugEnabled()) {
                log.debug("Socket was closed.");
            }
        } catch (IOException ex) {
            if (log.isDebugEnabled()) {
                log.debug("Error:********* " + ex);
            }
            JOptionPane.showMessageDialog(null, "There is no connection. Offline mode.");
        }
    }

    /**
     * ���������� ����������� �� �����
     *
     * @param imageTitle
     */
    private void showImage(String imageTitle) {        
        File imageFile = new File(DIR_IMAGES + "/" + imageTitle);
        //��� ������� �� NetBeans
        jLabel.setIcon(new javax.swing.ImageIcon(imageFile.toString()));
//        try {
//
//            //��� ������� jar
//            //    jLab.setIcon(new ImageIcon(ImageIO.read(getClass().getResource(file.toString()))));
//        } catch (IOException ex) {
//            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
//        }
        jLabel.setHorizontalAlignment(javax.swing.JLabel.CENTER);
        jScrollPane.getViewport().add(jLabel);
    }
    
    private void deleteFile(String dir,String imageTitle){
         File file = new File(dir + "/" + imageTitle);
         file.delete();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField addressInput;
    private javax.swing.JLabel currentCountLabel;
    private javax.swing.JTextField fileNameInput;
    private javax.swing.JLabel imageTitleLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JButton nextImageBtn;
    private javax.swing.JButton previousImgBtn;
    private javax.swing.JButton requestButton;
    private javax.swing.JLabel totalCountLabel;
    // End of variables declaration//GEN-END:variables
}